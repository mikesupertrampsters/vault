## Content

[![GitLabCI](https://gitlab.com/mikesupertrampsters/vault/badges/master/pipeline.svg)](https://gitlab.com/mikesupertrampsters/vault)
<a href="https://codeclimate.com/github/mikesupertrampster/vault/maintainability"><img src="https://api.codeclimate.com/v1/badges/19de0be80feedd25e9ca/maintainability" /></a>

Creates necessary AWS resources to deploy Vault.

## Usage

```bash
export TF_VAR_account_id=123456878910
export TF_VAR_env=dev
terragrunt [plan|apply]
```
