terragrunt = {
  remote_state = {
    backend = "s3"
    config {
      encrypt = true
      bucket = "mst-tfstate"
      key = "vault/env:/${get_env("TF_VAR_env", "")}/${path_relative_to_include()}/terraform.tfstate"
      region = "eu-west-1"
      dynamodb_table = "mst-${get_env("TF_VAR_env", "")}-lock"
    }
  }

  terraform = {
    extra_arguments "modules" {
      commands = ["get"]
      arguments = ["-update=true"]
    }

    extra_arguments "retry_lock" {
      commands = ["init", "apply", "refresh", "import", "plan", "taint", "untaint"]
      arguments = [
        "-lock-timeout=20m",
        "-var-file=${get_parent_tfvars_dir()}/environments/global.tfvars",
        "-var-file=${get_parent_tfvars_dir()}/environments/${get_env("TF_VAR_env", "")}/env.tfvars"
      ]
    }

    extra_arguments "destroy" {
      commands = ["destroy", "validate", "console"]
      arguments = [
        "-var-file=${get_parent_tfvars_dir()}/environments/global.tfvars",
        "-var-file=${get_parent_tfvars_dir()}/environments/${get_env("TF_VAR_env", "")}/env.tfvars"
      ]
    }

    extra_arguments "backups" {
      commands = ["apply"]
      arguments = ["-backup=-"]
    }
  }
}
