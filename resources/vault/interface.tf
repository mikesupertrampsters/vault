#------------------------------------------------------------------------------
# general
#------------------------------------------------------------------------------
variable account_id {}
variable azs { type = "list" }
variable env {}
variable project {}
variable provisioning_role {}
variable region {}

#------------------------------------------------------------------------------
# vault
#------------------------------------------------------------------------------
variable key_name { default = "dummy" }
variable letsencrypt_api {}
variable vault_asg_max {}
variable vault_asg_min {}
variable vault_instance_type {}

data "terraform_remote_state" "vpc" {
  backend = "s3"
  config {
    bucket = "${var.project}-tfstate"
    key    = "vault/env:/${var.env}/resources/vpc/terraform.tfstate"
    region = "${var.region}"
  }
}