provider "aws" {
  region = "${var.region}"

  assume_role {
    role_arn = "arn:aws:iam::${var.account_id}:role/${var.provisioning_role}"
  }
}

terraform {
  backend "s3" {}
}

data "aws_ami" "vault" {
  most_recent = true
  owners      = ["${var.account_id}"]

  filter {
    name   = "name"
    values = ["vault-*"]
  }

  filter {
    name   = "tag:ApprovedFor"
    values = ["*${var.env}*"]
  }
}

module "vault" {
//  source = "git::ssh://git@github.com/mikesupertrampster/terraform-modules//bundles/vault?ref=master"
  source              = "../../../../mst/_done/terraform-modules/bundles/vault"
  azs                 = "${var.azs}"
  env                 = "${var.env}"
  image_id            = "${data.aws_ami.vault.image_id}"
  instance_type       = "${var.vault_instance_type}"
  key_name            = "${var.key_name}"
  letsencrypt_api     = "${var.letsencrypt_api}"
  max_size            = "${var.vault_asg_max}"
  min_size            = "${var.vault_asg_min}"
  project             = "${var.project}"
  region              = "${var.region}"
  vpc_id              = "${data.terraform_remote_state.vpc.vpc_id}"
  vpc_zone_identifier = "${data.terraform_remote_state.vpc.private_subnets}"
}
